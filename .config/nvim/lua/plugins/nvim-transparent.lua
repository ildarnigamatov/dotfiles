return {
    {
      "xiyaowong/nvim-transparent",
      lazy = false,
      name = "transparent",
      config = function()
        require("transparent").setup {
        -- enable = true,
        groups = { -- table: default groups
          'Normal', 'NormalNC', 'Comment', 'Constant', 'Special', 'Identifier',
          'Statement', 'PreProc', 'Type', 'Underlined', 'Todo', 'String', 'Function',
          'Conditional', 'Repeat', 'Operator', 'Structure', 'LineNr', 'NonText',
          'SignColumn', 'CursorLineNr', 'EndOfBuffer', "NeoTreeNormal", "NeoTreeNormalNC", "NeoTreeUnderlined",
        },
        extra_groups = {

        }, -- table: additional groups that should be cleared
        exclude_groups = {}, -- table: groups you don't want to clear
        }
      end,
    },
}
