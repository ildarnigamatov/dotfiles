return {
  colorscheme = "monokai-nightasty",
  plugins = {
    {
      "polirritmico/monokai-nightasty.nvim",
      lazy = false,
      priority = 1000,
      init = function() -- init function runs before the plugin is loaded
        require("monokai-nightasty").setup({
            terminal_colors = false,
            hl_styles = {
                floats = "transparent", -- default, dark, transparent
                sidebars = "default", -- default, dark, transparent
            },
            on_colors = function(colors)
                colors.border = "#282a36"
                colors.comment = "#2d7e79"
                colors.bg_visual = "#282a36"
                colors.bg_statusline = "#282a36"
                colors.bg_popup = "#282a36"
                colors.grey_darker = "#282a36"
                colors.grey_dark = "#21232d"
            end,
        })
      end,
    },
  },
}
